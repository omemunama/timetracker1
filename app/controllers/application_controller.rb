class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  require 'csv'
  protect_from_forgery with: :exception

def only_admins
	redirect_to root_path, :alert => 'Only admin' unless current_user.admin
end

end
