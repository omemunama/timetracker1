class Usermailer < ApplicationMailer
	default from: "timetracker1@gmail.com"

	def workcreated_email(work)
		@work = work
		mail(to: work.project.owner.email, subject: "Work item posted")
	end

	def projectupdated_email(project)
		@project = project
		mail(to: project.owner.email, subject: "Project Update")
	end
end
